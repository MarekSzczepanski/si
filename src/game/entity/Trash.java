package game.entity;

import game.astar.Map;

import java.util.Random;

public class Trash {
	
	private int x;
	private int y;
	private int weight;
	private boolean visited;
	


	public Trash(int x, int y){
		this.x = x;
		this.y = y;
		Random generator = new Random();
		this.weight = generator.nextInt(5) + 1;
		this.visited = false;

	}
	

	public int getX(){
		return this.x;
	}
	
	public int getY(){
		return this.y;
	}
	
	public void setX(int x){
		this.x = x;
	}
	
	public void setY(int y){
		this.y = y;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}


	public boolean isVisited() {
		return visited;
	}

	public void setVisited(boolean visited) {
		this.visited = visited;
	}

	public double distanceTo(Trash pack){
		int distance = Map.findPath(this.getX(), this.getY(), pack.getX(), pack.getY()).size();
        
        return distance;
    }
	
	 
    @Override
    public String toString(){
        return getX()+", "+getY();
    }

}
