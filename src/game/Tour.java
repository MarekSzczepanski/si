package game;

import java.util.ArrayList;
import java.util.Collections;

import game.entity.Trash;

public class Tour{

    // Holds our tour of packages
    private ArrayList<Trash> tour = new ArrayList<Trash>();
    // Cache
    private double fitness = 0;
    private int distance = 0;
    
    // Constructs a blank tour
    public Tour(){
        for (int i = 0; i < TourManager.numberOfTrashs(); i++) {
            tour.add(null);
        }
    }
    
    public Tour(ArrayList tour){
        this.tour = tour;
    }

    // Creates a random individual
    public void generateIndividual() {
        // Loop through all our destination cities and add them to our tour
        for (int packIndex = 0; packIndex < TourManager.numberOfTrashs(); packIndex++) {
          setTrash(packIndex, TourManager.getTrash(packIndex));
        }
        // Randomly reorder the tour
        Collections.shuffle(tour);
    }

    // Gets a city from the tour
    public Trash getTrash(int tourPosition) {
    	try {
    		return (Trash)tour.get(tourPosition);
    	}catch (Exception e) {
			System.out.println("End");
			return null;
		}
    }

    // Sets a city in a certain position within a tour
    public void setTrash(int tourPosition, Trash pack) {
        tour.set(tourPosition, pack);
        // If the tours been altered we need to reset the fitness and distance
        fitness = 0;
        distance = 0;
    }
    
    // Gets the tours fitness
    public double getFitness() {
        if (fitness == 0) {
            fitness = 1/(double)getDistance();
        }
        return fitness;
    }
    
    // Gets the total distance of the tour
    public int getDistance(){
        if (distance == 0) {
            int tourDistance = 0;
            // Loop through our tour's packages
            for (int packIndex=0; packIndex < tourSize(); packIndex++) {
                // Get Trashage we're travelling from
                Trash fromTrash = getTrash(packIndex);
                // Trashage we're travelling to
                Trash destinationTrash;
                // Check we're not on our tour's last package, if we are set our 
                // tour's final destination package to our starting package
                if(packIndex+1 < tourSize()){
                    destinationTrash = getTrash(packIndex+1);
                }
                else{
                    destinationTrash = getTrash(0);
                }
                // Get the distance between the two packages
                tourDistance += fromTrash.distanceTo(destinationTrash);
            }
            distance = tourDistance;
        }
        return distance;
    }

    // Get number of packages on our tour
    public int tourSize() {
        return tour.size();
    }
    
    // Check if the tour contains a package
    public boolean containsTrash(Trash pack){
        return tour.contains(pack);
    }
    
    public void remove(int x){
    	 tour.remove(x);
    }
    
    @Override
    public String toString() {
        String geneString = "|";
        for (int i = 0; i < tourSize(); i++) {
            geneString += getTrash(i)+"|";
        }
        return geneString;
    }
}
