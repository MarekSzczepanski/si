package game;

import java.util.ArrayList;

import game.entity.Trash;

public class TourManager {
	
	  // Holds our packages
    public static ArrayList<Trash> destinationPlaces = new ArrayList<Trash>();

    // Adds a destination package
    public static void addTrash(Trash pack) {
    	destinationPlaces.add(pack);
    }

    public static void deleteTrash(Trash trash){
        destinationPlaces.remove(trash);
    }
    
    // Get a package
    public static Trash getTrash(int index){
        return (Trash)destinationPlaces.get(index);
    }
    
    // Get the number of destination packages
    public static int numberOfTrashs(){
        return destinationPlaces.size();
    }

}
