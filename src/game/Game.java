package game;

import game.astar.Map;
import game.astar.Node;
import game.entity.Player;
import game.entity.Trash;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class Game extends JPanel implements MouseListener
{

	private Map map;
	private Player player;
	private Trash trash1;
	private Trash trash2;
	private Trash trash3;
	private Trash trash4;
	private List<Node> path;
	private Image imageW;
	private Image imageN;
	private Image imageE;
	private Image imageS;

	private Image trash;
	private Tour tour;

	int[][] m0 =  	{// 0	 1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18
					{ 4, 4, 1, 1, 1, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},//0
					{ 1, 1, 6, 1, 6, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 1, 1, 1},//1
					{ 6, 1, 6, 1, 1, 1, 1, 1, 1, 6, 6, 1, 1, 1, 1, 6, 1, 6, 6},//2
					{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 6, 1, 1, 1, 1, 1, 1, 1, 1},//3
					{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},//4
					{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},//5
					{ 1, 1, 1, 1, 1, 1, 1, 1, 6, 6, 6, 1, 1, 1, 1, 1, 1, 1, 1},//6
					{ 1, 1, 1, 1, 1, 1, 1, 1, 6, 1, 6, 1, 1, 1, 1, 1, 1, 1, 1}, //7
					{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},//8
					{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},//9
					{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, //10
					{ 1, 1, 6, 6, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},//11
					{ 1, 1, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 1, 1},//12
					{ 1, 1, 6, 1, 1, 1, 1, 1, 1, 6, 1, 1, 1, 1, 1, 1, 6, 1, 1},//13
					{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 1, 4, 4, 1, 1, 1, 6, 1, 1},//14
					{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 1, 4, 4, 1, 1, 1, 1, 6, 1},//15
	};


	public Game()
	{
		int[][] m = m0;

		setPreferredSize(new Dimension(m[0].length * 60, m.length * 60));
		addMouseListener(this);

		
		try {
			imageW = ImageIO.read(new File("input/vehicle.png"));
			imageN = ImageIO.read(new File("input/vehicleN.png"));
			imageE = ImageIO.read(new File("input/vehicleE.png"));
			imageS = ImageIO.read(new File("input/vehicleS.png"));
			trash = ImageIO.read(new File("input/trash.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		map = new Map(m);
		player = new Player(9, 7);

	
		trash1 = new Trash(18,0);
		trash2 = new Trash(0,15);
		trash3 = new Trash(3,0);
		trash4 = new Trash(18,15);

		TourManager.addTrash(trash1);
		TourManager.addTrash(trash2);
		TourManager.addTrash(trash3);
		TourManager.addTrash(trash4);
		
		  Population pop = new Population(50, true);

	        // Evolve population for 100 generations
	        pop = GA.evolvePopulation(pop);
	        for (int i = 0; i < 100; i++) {
	            pop = GA.evolvePopulation(pop);
	            
	           
	        }
	        
	        setTour(pop.getFittest());

	        // Print final results
	        System.out.println("Distance: " + getTour().getDistance());
	        System.out.println("Solution:");
	        System.out.println(getTour());
	        
	    }

	public void update()
	{
		player.update();

		if(compareLocation(player.getX(),trash1.getX()) && compareLocation(player.getY(),trash1.getY() )){


			if(!trash1.isVisited()){
					try {
						path = map.findPath(player.getX(), player.getY(), getTour().getTrash(0).getX(), getTour().getTrash(0).getY());
						printRoute(path);
						player.followPath(path);
						getTour().remove(0);
						trash1.setVisited(true);
						player.setCapacity(player.getCapacity() - trash1.getWeight());
						System.out.println("Car capacity: " + player.getCapacity() + "/10");

						if(getTour().tourSize() == 0 || player.getCapacity()< getTour().getTrash(0).getWeight()){
							delivertrash(trash1);
							player.setCapacity(10);
						}
					} catch (NullPointerException ex) {
					}
				}
		}

		if(compareLocation(player.getX(),trash2.getX()) && compareLocation(player.getY(),trash2.getY()) ){
			if(!trash2.isVisited()){
					try {
						path = map.findPath(player.getX(), player.getY(), getTour().getTrash(0).getX(), getTour().getTrash(0).getY());
						printRoute(path);
						player.followPath(path);
						getTour().remove(0);
						trash2.setVisited(true);
						player.setCapacity(player.getCapacity() - trash2.getWeight());
						System.out.println("Car capacity: " + player.getCapacity() + "/10");

						if(getTour().tourSize() == 0 || player.getCapacity()< getTour().getTrash(0).getWeight()){
							delivertrash(trash2);
							player.setCapacity(10);
						}
					} catch (NullPointerException ex) {
					}
			}
		}

		if(trash3 != null && compareLocation(player.getX(),trash3.getX()) && compareLocation(player.getY(),trash3.getY())){
			if(!trash3.isVisited()){

					try {
						path = map.findPath(player.getX(), player.getY(), getTour().getTrash(0).getX(), getTour().getTrash(0).getY());
						printRoute(path);
						player.followPath(path);
						getTour().remove(0);
						trash3.setVisited(true);
						player.setCapacity(player.getCapacity() - trash3.getWeight());
						System.out.println("Car capacity: " + player.getCapacity() + "/10");

						if(getTour().tourSize() == 0 || player.getCapacity()< getTour().getTrash(0).getWeight()){
							delivertrash(trash3);
							player.setCapacity(10);

						}
					} catch (NullPointerException ex) {

					}

				}


			}

		if(compareLocation(player.getX(),trash4.getX()) && compareLocation(player.getY(),trash4.getY())){
			if(!trash4.isVisited()){
					try {
						path = map.findPath(player.getX(), player.getY(), getTour().getTrash(0).getX(), getTour().getTrash(0).getY());
						printRoute(path);
						player.followPath(path);
						getTour().remove(0);
						trash4.setVisited(true);
						player.setCapacity(player.getCapacity() - trash1.getWeight());
						System.out.println("Car capacity: " + player.getCapacity() + "/10");

						if(getTour().tourSize() == 0 || player.getCapacity()< getTour().getTrash(0).getWeight()){
							delivertrash(trash4);

							player.setCapacity(10);
						}
					} catch (NullPointerException ex) {

					}
			}
		}
	}
		


	public void render(Graphics2D g)
	{
		map.drawMap(g, path, m0);
		g.setColor(Color.GRAY);
		for (int x = 0; x < getWidth(); x += 60)
		{
			g.drawLine(x, 0, x, getHeight());
		}
		for (int y = 0; y < getHeight(); y += 60)
		{
			g.drawLine(0, y, getWidth(), y);
		}
		if(!trash1.isVisited()) {
			g.drawImage(trash, trash1.getX() * 60 + 1, trash1.getY() * 60 + 1, 59, 59, null, null);
		}
		if(!trash2.isVisited()) {
			g.drawImage(trash, trash2.getX() * 60 + 1, trash2.getY() * 60 + 1, 59, 59, null, null);
		}
		if(!trash3.isVisited()) {
			g.drawImage(trash, trash3.getX() * 60 + 1, trash3.getY() * 60 + 1, 59, 59, null, null);
		}
		if(!trash4.isVisited()) {
			g.drawImage(trash, trash4.getX() * 60 + 1, trash4.getY() * 60 + 1, 59, 59, null, null);
		}

		//g.drawImage(imageW, player.getX() * 60 + 1 + player.getSx(), player.getY() * 60 + 1 + player.getSy(), 59, 59, Color.WHITE, null);

		if(path!=null && path.size()>1) {
				Node current = path.get(1);
				Node previous = path.get(0);
				if (current.getX() + 1 == previous.getX() && current.getY() == previous.getY()) {
					player.setDirection("W");
				} else if (current.getX() - 1 == previous.getX() && current.getY() == previous.getY()) {

					player.setDirection("E");
				} else if (current.getY() + 1 == previous.getY() && current.getX() == previous.getX()) {

					player.setDirection("N");
				} else if (current.getY() - 1 == previous.getY() && current.getX() == previous.getX()) {

					player.setDirection("S");
				}
		}
				if(player.getDirection().equals("N")){
			g.drawImage(imageN, player.getX() * 60 + 1 + player.getSx(), player.getY() * 60 + 1 + player.getSy(), 59, 59, Color.WHITE, null);
		} else if(player.getDirection().equals("W")) {
					g.drawImage(imageW, player.getX() * 60 + 1 + player.getSx(), player.getY() * 60 + 1 + player.getSy(), 59, 59, Color.WHITE, null);
		} else if(player.getDirection().equals("E")) {
			g.drawImage(imageE, player.getX() * 60 + 1 + player.getSx(), player.getY() * 60 + 1 + player.getSy(), 59, 59, Color.WHITE, null);
		} else {
			g.drawImage(imageS, player.getX() * 60 + 1 + player.getSx(), player.getY() * 60 + 1 + player.getSy(), 59, 59, Color.WHITE, null);
		}
	}

	@Override
	public void mouseClicked(MouseEvent e)
	{
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
	}

	@SuppressWarnings("static-access")
	@Override
	public void mousePressed(MouseEvent e)
	{
		
		try {
			path = map.findPath(player.getX(), player.getY(), getTour().getTrash(0).getX(),getTour().getTrash(0).getY());
			printRoute(path);
			player.followPath(path);
		}catch (NullPointerException ex) {
			
		}
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
	}
	
	public boolean compareLocation(int x , int y){
		if(x==y) return true;
		else return false;
	}
	
	
	public int [] nearestPoint(int px, int py,int color){
		
		int minDist = 1000;
		int tempX = 0,tempY = 0;
		int destX = 0, destY = 0;
		
		
		for(int y=0;y<m0.length;y++){
			for(int x=0; x<m0[0].length; x++){
				int dist = 0;
				if(m0[y][x]!=0 && m0[y][x] == color){
					if (x+1<m0[0].length && map.getNode(x+1, y).isWalkable())
					{
						dist = map.findPath(px, py, x+1,y).size();
						if(minDist>dist){
							tempX = x+1;
							tempY = y;
							destX = x;
							destY = y;
							minDist = dist;
						}	
					
					}
					else if (x-1>= 0 && map.getNode(x-1,y).isWalkable())
					{
						dist = map.findPath(px, py, x-1, y).size();
					
						if(minDist>dist){
							tempX = x-1;
							tempY = y;
							destX = x;
							destY = y;
							minDist = dist;
						}	
					}
					else if (y+1<m0.length && map.getNode(x, y+1).isWalkable() )
					{
						dist = map.findPath(px, py,x, y+1).size();
						if(minDist>dist){
							tempX = x;
							tempY = y+1;
							destX = x;
							destY = y;
							minDist = dist;
						}	
					
					}
					else if (y-1>=0 && map.getNode(x, y-1).isWalkable())
					{
						dist = map.findPath(px, py, x, y-1).size();
						if(minDist>dist){
							tempX = x;
							tempY = y-1;
							destX = x;
							destY = y;
							minDist = dist;
						}	
					
					}
								
			}
		}
		
		}
		int tab[] = {tempX,tempY,destX,destY};
		return tab;
	}
	
	public void printRoute(List<Node> path){
/*		for(int i = 1; i<path.size();i++){
			Node current = path.get(i);
			Node previous = path.get(i-1);
			if(current.getX()+1==previous.getX() && current.getY()==previous.getY()) {
				System.out.println("West");
			}
			else if(current.getX()-1==previous.getX()&&current.getY()==previous.getY()){
				System.out.println("East");
			}
			else if(current.getY()+1==previous.getY()&&current.getX()==previous.getX()){
				System.out.println("North");
			}
			else if(current.getY()-1==previous.getY()&&current.getX()==previous.getX()){
				System.out.println("South");
			}
		}*/
	}
	
	public void delivertrash(Trash trash) {
		int resultP[] = null;
		resultP = nearestPoint(trash.getX(),trash.getY(),4);


		System.out.println("Deliver trash to dump. X: "+resultP[0]+", Y: "+resultP[1]);
		path = map.findPath(player.getX(), player.getY(), resultP[0], resultP[1]);
		printRoute(path);
		player.followPath(path);
	}


	public Tour getTour() {
		return tour;
	}


	public void setTour(Tour tour) {
		this.tour = tour;
	}

}